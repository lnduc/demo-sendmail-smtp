﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Net;
using System.Net.Mail;

namespace Gmail
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            
        }

        private void btn_TestSendMail_Click(object sender, EventArgs e)
        {
            MailAddress myemail = new MailAddress(txt_Username.Text, "Sender Name");
            MailAddress mail_to = new MailAddress(txt_Receivermail.Text, "Receiver Name");

            string password = txt_Password.Text;

            SmtpClient client_smtp = new SmtpClient("smtp.gmail.com", 587);
            client_smtp.EnableSsl = true;
            client_smtp.DeliveryMethod = SmtpDeliveryMethod.Network;
            client_smtp.UseDefaultCredentials = false;
            client_smtp.Credentials = new NetworkCredential(myemail.Address, password);

            MailMessage message = new MailMessage(myemail, mail_to);
            message.Subject = "TEST SEND MAIL FROM C# APP";
            message.Body = "THIS IS TEST CONTENT";

            try
            {
                client_smtp.Send(message);
                lb_messages.Text = "Đã gửi thành công!";
            }
            catch (Exception ex)
            {
                lb_messages.Text=ex.ToString();
            }
        }
    }
}
